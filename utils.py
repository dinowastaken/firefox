from subprocess import Popen, run
import wget

def process(cmd) -> Popen:
    return Popen(cmd.split(" "))


def waitall(process: []):
    for i in process:
        i.wait()

def downloadTo(url, path):
    print(f"=======> downloading {url}")
    print(f"=========> Spawning wget {url} -O {path}")
    run(["wget", 
        # "-q", "--show-progress", 
        url, "-O", path])
