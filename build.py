from utils import process, waitall, downloadTo
import re

data = None
config_version = None

with open('./pkg.yml', 'r') as f:
    data = f.read()
    _all = re.findall("version: (\d.*)", data)

    for s in _all:
        config_version = s
        break

# Use the base url to get the location(header), with the location, you can get the version
baseurl = "https://download.mozilla.org/?product=firefox-latest&lang=en-US"

def getversion():
    from requests import get 
    print("=====> getting version")
    res = get(baseurl, allow_redirects=False)
    print("=====> got version")
    return res.text.split("/")[6] # returns the version from the body that is <a href="https://download-installer.cdn.mozilla.net/pub/firefox/releases/{VERSION}/win32/en-US/Firefox%20Setup%208{VERSION}.exe">Found</a>.

version = getversion()
if version == config_version:
    print(f"{version} is already Packed")
    exit(-1)

print(f"packing new version {version}, current version {config_version}")

# Predownload

print("=====> Start PreDownload")
process("rm -rf tmp").wait()

waitall([
    process("mkdir -p tmp/linux"),
    process("mkdir -p tmp/osx"),
    process("mkdir -p tmp/win"),
    process("mkdir -p tmp/opt"),
])

print("=====> Done PreDownload")

# Download
print("=====> Start DOWNLOADING")

downloadTo(f"https://ftp.mozilla.org/pub/firefox/releases/{version}/win64/en-US/Firefox%20Setup%20{version}.exe", "tmp/opt/win.exe")
downloadTo(f"https://ftp.mozilla.org/pub/firefox/releases/{version}/mac/en-US/Firefox%20{version}.dmg", "tmp/opt/osx.dmg")
downloadTo(f"https://ftp.mozilla.org/pub/firefox/releases/{version}/linux-x86_64/en-US/firefox-{version}.tar.bz2", "tmp/opt/linux.tar.bz2")

print("=====> Done DOWNLOADING")

# Post Download

print("=====> Start PostDownload")

# extract files to tmp/{SYS}/app 
waitall([
    process("tar xf tmp/opt/linux.tar.bz2 -C tmp/opt/"),
    process("7z x tmp/opt/win.exe -y -otmp/opt/win.unpacked "),
    process("7z x tmp/opt/osx.dmg -y -otmp/opt/osx.unpacked "),
])
process("mv tmp/opt/firefox tmp/linux/app").wait()
process("mv tmp/opt/win.unpacked/core tmp/win/app").wait() 
process("mv tmp/opt/osx.unpacked/Firefox/Firefox.app tmp/osx/app").wait() 
process("rm -rf tmp/opt").wait()

# remove updater
waitall([
    process("rm -r tmp/linux/app/updater tmp/linux/app/updater.ini tmp/linux/app/update-settings.ini tmp/linux/app/removed-files tmp/linux/app/precomplete"),
    process("rm -r tmp/osx/app/Contents/MacOS/updater.app tmp/osx/app/Contents/Resources/precomplete tmp/osx/app/Contents/Resources/removed-files"),
    process("rm -r tmp/win/app/updater.exe tmp/win/app/updater.ini tmp/win/app/removed-files tmp/win/app/precomplete tmp/win/app/uninstall"),
])
    
print("=====> Done Extracting; Making final Tuches")

# set default preferences
waitall([
    process("cp -r distribution tmp/linux/app/distribution"),
    process("cp -r distribution tmp/osx/app/Contents/Resources/distribution"),
    process("cp -r distribution tmp/win/app/distribution"),
])
print("=====> Preferences Set")


print("=====> app.sh generated")
waitall([
    process("cp app.lin.sh tmp/linux/app.sh"),
    process("cp app.win.sh tmp/win/app.sh"),
    process("cp app.osx.sh tmp/osx/app.sh"),
])
print("=====> Done PostDownload")
print("=====> Writing the new pkg.yml")
data = data.replace(f"version: {s}", f"version: {version}")

    
with open('./pkg.yml', 'w') as f:
    f.write(data)